package com.isima;

import java.util.ArrayList;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

/**
 * Classe qui modélise une intersection. Elle connecte plusieurs routes sous 
 * la forme d'un carrefour à sens giratoire.
 */
public class Intersection
{
    protected double x;
    protected double y;
    protected double taille;

    protected int id;

    protected ArrayList<Route> routeList;
    protected ArrayList<Voiture> voituresListe;

    public boolean estLibre;

    private Logger logger = (Logger) LoggerFactory.getLogger("com.isima.Intersection");

    Intersection(double x, double y)
    {
        this.x = x;
        this.y = y;
        this.id = DistributeurId.getNewId();
        taille = 15;

        routeList = new ArrayList<Route>();
        voituresListe = new ArrayList<Voiture>();

        estLibre = true;
    }

    /**
     * @return l'identifiant de l'intersection
     */
    public int getId(){
        return id;
    }

    /**
     * @return la liste des routes qui sont connectées à l'intersection
     */
    public ArrayList<Route> getRouteList() {
        return routeList;
    }

    /**
     * @return l'abscisse du centre de l'intersection
     */
    public double getX() {
        return x;
    }

    /**
     * @return l'ordonnée du centre de l'intersection
     */
    public double getY() {
        return y;
    }

    /**
     * @return le rayon de l'intersection
     */
    public double getTaille() {
        return taille;
    }

    /**
     * @return la liste des voitures présentes sur l'intersection
     */
    public ArrayList<Voiture> getVoituresListe(){
        return voituresListe;
    }

    /**
     * Permet de mettre à jour la liste des voitures présentes sur l'intersection
     * @param routeList : la liste des voitures
     */
    public void setRouteList(ArrayList<Route> routeList) {
        this.routeList = routeList;
    }

    /**
     * Ajoute une route à l'intersection
     * @param route : la route à ajouter
     */
    public void addRouteListe(Route route){
        this.routeList.add(route);
    }

    /**
     * Modifie l'abscisse du centre de l'intersection
     * @param x : nouvelle abscisse
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * Modifie l'ordonnée du centre de l'intersection
     * @param y : nouvelle ordonnée
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * Modifie le rayon de l'intersection
     * @param taille : nouveau rayon
     */
    public void setTaille(double taille) {
        this.taille = taille;
    }

    /**
     * Ajoute une voiture dans l'intersection
     * @param voiture : la voiture à ajouter
     */
    public void addVoiture(Voiture voiture){
        voituresListe.add(voiture);
    }

    /**
     * Enlève une voiture dans l'intersection
     * @param voiture : la voiture à enlever
     */
    public void removeVoiture(Voiture voiture){
        voituresListe.remove(voiture);
    }

    @Override
    public String toString(){
        return "Intersection " + this.id;
    }

    /**
     * Détermine si une portion d'intersection contient au moins une voiture
     * @param debut : angle à partir duquel on regarde la présence de voitures
     * @param portee : angle qui détermine la portion d'intersection à examiner
     * à partir du paramètre debut dans le sens de circulation des voitures
     * @return si la voie est libre ou pas
     */
    public boolean estDegagee(double debut, double portee)
    {
        boolean res = true;
        double delta_angle;

        for (Voiture v : voituresListe)
        {
            delta_angle = v.getAvancementIntersection() - debut;

            // Normalisation de l'angle entre ]-pi;pi]
            while (delta_angle <= - Math.PI) delta_angle += 2 * Math.PI;
            while (delta_angle >    Math.PI) delta_angle -= 2 * Math.PI;

            if (delta_angle > 0 && delta_angle < portee) res = false;
        }

        return res;
    }

    /**
     * Renvoie l'angle que fait une route avec l'intersection
     * @param route : une route qui est connectée à l'intersection
     * (sinon le résultat n'aura pas de sens)
     * @return
     */
    public double angleRoute(Route route)
    {   
        Intersection autre_intersetion = null;

        if (route.intersectionA.getId() == this.getId())
            autre_intersetion = route.intersectionB;
        else if (route.intersectionB.getId() == this.getId())
            autre_intersetion = route.intersectionA;
        else
            logger.error("[Intersection.angleRoute] Erreur : la route n'est pas connectee a l'intersection");

        double res = Math.atan2(
            autre_intersetion.y - this.y,
            autre_intersetion.x - this.x);

        return res;
    }
}
package com.isima;

import java.util.ArrayList;
import java.util.Random;
import java.lang.Math;

import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.Logger;


enum Avancer {Route, AvantIntersection, Intersection}

/**
 * Classe qui modélise un voiture capable d'observer son environnement et 
 * ses semblables
 */
public class Voiture
{   
    private static Random random = new Random((long)1658717231);

    // Permet de normaliser la vitesse des voitures sur la carte par rapport
    // à la vitesse d'execution du programme. Par exemple, si les voitures 
    // avancent 20 fois par seconde alors le paramètre vaut 20. C'est un attribut
    // de classe puisqu'il est commun à toutes les voitures.
    private static double rapportDeVitesse = 1.0; 

    private Logger logger = (Logger) LoggerFactory.getLogger("com.isima.logback.voiture");
    

    // Il faudra rajouter une taille pour la voiture
    private double avancementRoute;
    private double avancementIntersection;
    private double vitesse;
    private double vitesseActuelle;
    private Route routeActuelle;
    private Route routeAncienne;
    private Intersection intersectionDepart;
    private Intersection intersectionArrivee;
    private int id;
    // On considère que la position de la voiture définit son capot
    private double taille; 

    private Avancer etat = Avancer.Route;

    // Patron adaptateur ?
    // https://stackoverflow.com/questions/4280727/java-creating-an-array-of-methods
    private interface ObservateurEnvironnement{
        public double getVitessePreconisee();
    }

    private ObservateurEnvironnement[] observateursRoute = new ObservateurEnvironnement[]{
        new ObservateurEnvironnement() {public double getVitessePreconisee(){
            return regarderVoitureDevantRoute();}},

        new ObservateurEnvironnement() {public double getVitessePreconisee(){
            return regarderIntersectionRoute();}}
    };

    Voiture(double vitesse, Route routeActuelle, Intersection intersectionDepart, Intersection intersectionArrivee)
    {
        this.vitesse = vitesse;
        this.vitesseActuelle = vitesse;
        this.avancementRoute = 0.0;
        this.routeActuelle = routeActuelle;
        this.intersectionDepart = intersectionDepart;
        this.intersectionArrivee = intersectionArrivee;
        this.id = DistributeurId.getNewId();
        this.taille = 7;

        this.routeActuelle.addVoiture(this);

        avancementIntersection = -100;

        
        logger.debug("vitesse : " + vitesse);
        logger.debug("routeActuelle : " + routeActuelle.toString());
    }

    /**
     * @return le rapport de vitesse utilisé pour les animations
     */
    static public double getRapportDeVitesse(){
        return rapportDeVitesse;
    }

    /**
     * Modifie le rapport de vitesse utilisé pour les animations
     * @param rapport : le nouveau rapport
     */
    static public void setRapportDeVitesse(double rapport){
        rapportDeVitesse = rapport;
    }

    /**
     * @return l'identifiant de la voiture
     */
    public int getId(){
        return id;
    }

    /**
     * @return la distance de la voiture par rapport au début de la route
     * (qui n'a du sens qui si la voiture est sur une route)
     */
    public double getAvancementRoute() {
        return avancementRoute;
    }

    /**
     * @return la position angulaire de la voiture par rapport à l'intersection
     * (qui n'a du sens que si la voiture est sur une intersection)
     */
    public double getAvancementIntersection(){
        return avancementIntersection;
    }

    /**
     * @return la vitesse de croisière thèorique de la voiture
     */
    public double getVitesse(){
        return vitesse;
    }

    /**
     * @return la vitesse que possède la voiture réellement
     */
    public double getVitesseAct(){
        return vitesseActuelle;
    }

    /**
     * @return la route sur laquelle se situe la voiture
     */
    public Route getRouteActuelle(){
        return routeActuelle;
    }

    /**
     * @return l'intersection d'où provient la voiture
     */
    public Intersection getIntersectionDepart(){
        return intersectionDepart;
    }

    /**
     * @return l'intersection vers laquelle se dirige la voiture
     */
    public Intersection getIntersectionArrivee(){
        return intersectionArrivee;
    }

    /**
     * @return la longueur de la voiture
     */
    public double getTaille(){
        return taille;
    }

    /**
     * Positionne la voiture sur sa route actuelle
     * @param avancementRoute : distance depuis l'intersection de départ
     */
    public void setAvancementRoute(double avancementRoute) {
        this.avancementRoute = avancementRoute;
    }

    /**
     * Modifie la vitesse théorique de la voiture
     * @param vitesse
     */
    public void setVitesse(double vitesse){
        this.vitesse = vitesse;
    }

    /**
     * Modifie la vitesse avec laquelle la voiture va vraiment avancer
     * @param vitesseActuelle
     */
    public void setVitesseAct(double vitesseActuelle){
        this.vitesseActuelle = vitesseActuelle;
    }

    @Override
    public String toString(){
        return "Voiture " + this.id;
    }

    /**
     * Fait avancer la voiture. C'est normalement la seule vrai méthode 
     * publique de cette classe qui donne vie à la voiture puisque qu'on veut 
     * que celle-ci agisse de façon autonome.
     */
    public void avancer()
    {
        switch (etat) {
            case Route:
                regulerVitesseRoute();
                avancerRoute();
                break;
            
            case AvantIntersection:
                regulerVitesseRoute();
                avancerAvantIntersection();
                if (this.etat != Avancer.Intersection) break;

            case Intersection:
                avancerIntersection();
                break;
        
            default:
                break;
        }
    }

    /**
     * Fait en sorte que la voiture adapte sa vitesse en fonction de son environnement
     */
    private void regulerVitesseRoute()
    {   
        final double ACCELERATION_MAX = 5;
        double vitesse_preconisee, min_vitesse_preconisee = Double.POSITIVE_INFINITY;

        for (ObservateurEnvironnement observateur : observateursRoute)
        {
            vitesse_preconisee = observateur.getVitessePreconisee();

            if (vitesse_preconisee < min_vitesse_preconisee)
                min_vitesse_preconisee = vitesse_preconisee;
        }

        if (min_vitesse_preconisee < Double.POSITIVE_INFINITY)
        {   
            if (Math.abs(vitesseActuelle - min_vitesse_preconisee) < ACCELERATION_MAX)
                vitesseActuelle = min_vitesse_preconisee;

            else if (vitesseActuelle > min_vitesse_preconisee)
                vitesseActuelle -= ACCELERATION_MAX;
            else 
                vitesseActuelle += ACCELERATION_MAX;

        }
        else // s'il n'y a aucune raison de freiner, on accélère
        {
            vitesseActuelle += ACCELERATION_MAX / 2; // On accelère pas trop vite non plus

            if (vitesseActuelle > routeActuelle.getVitesseMaxAutorisee()){
                vitesseActuelle = routeActuelle.getVitesseMaxAutorisee();
            }
        }
    }

    /**
     * Regarde la voiture qui est la plus proche devant et adapte la vitesse 
     * éviter de lui rentrer dedans.
     * @return la vitesse preconisée pour l'élément observé
     */
    private double regarderVoitureDevantRoute()
    {
        final double DIST_MIN_ENTRE_VOITURES = 10; // On ne veut pas que les voitures se rentrent dedans

        double vitesse_preconisee = Double.POSITIVE_INFINITY;

        // Recherche de la voiture la plus proche roulant dans le même sens
        // que notre objet voiture "this"
        Voiture voiture_proche = null;
        double avancement_minimal = routeActuelle.getDistance() + 100;

        
        for (Voiture voiture : routeActuelle.getVoituresListe())
        {
            if (voiture.getAvancementRoute() > this.getAvancementRoute() &&
                voiture.getIntersectionArrivee() == this.getIntersectionArrivee() &&
                voiture.getAvancementRoute() < avancement_minimal)
            {   
                voiture_proche = voiture;
                avancement_minimal = voiture.getAvancementRoute();
            }
        }

        if (voiture_proche != null) // On regarde si on a trouvé une voiture devant
        {
            double distance_securite = 2 * vitesseActuelle;
            double diff_distance = voiture_proche.getAvancementRoute() - voiture_proche.getTaille() - this.getAvancementRoute();

            if (distance_securite < DIST_MIN_ENTRE_VOITURES) // On ne roule pas vite
            {   
                
                if (diff_distance < DIST_MIN_ENTRE_VOITURES) // On est trop proche, on s'arrête
                {   
                    vitesse_preconisee = 0.0;
                }
                else
                {
                    vitesse_preconisee = 30 * (diff_distance - DIST_MIN_ENTRE_VOITURES) + 20;
                } 
                    
                // Sinon on ne préconise rien comme vitesse
            }
            else // On roule à une certaine vitesse
            {   
                if (diff_distance < distance_securite) // On est trop proche, on freine
                {
                    vitesse_preconisee = diff_distance / 1.5 ;// Devrait être 2 normalement
                }

                // Sinon on ne préconise rien comme vitesse
            }
        }
        return vitesse_preconisee;
    }

    /**
     * Regarde s'il est possible de s'engager sur l'intersection à venir
     * @return la vitesse preconisée pour l'élément observé
     */
    private double regarderIntersectionRoute()
    {   
        final double DIST_VISION_INTERSECTION = 50;

        double vitesse_preconisee = Double.POSITIVE_INFINITY;
        double dist_avant_intersection = routeActuelle.getDistance() - getAvancementRoute();
        

        if (dist_avant_intersection < DIST_VISION_INTERSECTION)
        {   
            boolean est_libre;
            est_libre = intersectionArrivee.estDegagee(intersectionArrivee.angleRoute(routeActuelle) - 1, 2);
            if (!est_libre)
            {   
                vitesse_preconisee = 40 * dist_avant_intersection / DIST_VISION_INTERSECTION + 5;
            }
            else
            {   
                vitesse_preconisee = 40 * dist_avant_intersection / DIST_VISION_INTERSECTION + 15;
            }
        }

        return vitesse_preconisee;
    }

    /**
     * Fait avancer la voiture quand celle-ci est sur une route
     */
    private void avancerRoute()
    {
        double delta_distance = vitesseActuelle / rapportDeVitesse;

        if (avancementRoute + delta_distance < routeActuelle.getDistance())
        {
            avancementRoute += delta_distance;
        }
        else // Si on arrive à la fin de la route
        {
            avancementRoute = routeActuelle.getDistance();
            etat = Avancer.AvantIntersection;
        }
    }

    /**
     * Fait "avancer" la voiture quand celle-ci est en train de patienter pour 
     * s'engager sur une intersection
     */
    private void avancerAvantIntersection()
    {
        logger.debug("avancerAvantIntersection");
        logger.debug("routeActuelle : {}", routeActuelle);

        // Choix route suivante
        ArrayList<Route> routeListe = intersectionArrivee.getRouteList();
        // Pour ne pas qu'on retourne sur la même route :

        Route routeSuivante;
        int i = 0;

        do {
            int idNouvelleRoute = random.nextInt(routeListe.size());
            routeSuivante = routeListe.get(idNouvelleRoute);
            i++;
        } while (routeSuivante.getId() == routeActuelle.getId() && i < 10);


        // On vérifie que l'intersection est libre
        boolean est_libre;
        est_libre = intersectionArrivee.estDegagee(intersectionArrivee.angleRoute(routeActuelle) - 1, 2);
        if (est_libre)
        {   
            intersectionArrivee.estLibre = false;
            // On passe dans l'intersection : intersectionArrivee

            routeActuelle.removeVoiture(this);

            intersectionArrivee.addVoiture(this);
            routeAncienne = routeActuelle;
            routeActuelle = routeSuivante;
            
            etat = Avancer.Intersection;
            logger.debug("Passage sur l'intersection : {}", intersectionArrivee);
        }
        else
        {
            vitesseActuelle = 0;
        }
    }


    /**
     * Fait avancer une voiture quand celle-ci est sur une intersection
     */
    private void avancerIntersection()
    {   
        vitesseActuelle = vitesse / 2;

        if (avancementIntersection < -99){ // avancementIntersection n'es pas défini   
            avancementIntersection = intersectionArrivee.angleRoute(routeAncienne);
        }
        
        // RouteActuelle est pour le moment routeSuivante...
        double position_angulaire_sortie = intersectionArrivee.angleRoute(routeActuelle);

        // Petite correction pour un mouvement plus réaliste
        position_angulaire_sortie += 2 * Math.asin(routeActuelle.getLargeur() / (4 * intersectionArrivee.getTaille()));

        double angle_avancement = (vitesseActuelle / rapportDeVitesse) / intersectionArrivee.getTaille() * 0.7;
        
        double delta_angle = position_angulaire_sortie - avancementIntersection;
        while (delta_angle < 0)          delta_angle += 2*Math.PI;
        while (delta_angle >= 2*Math.PI) delta_angle -= 2*Math.PI;

        double delta_angle_avancement = position_angulaire_sortie - avancementIntersection - angle_avancement;
        while (delta_angle_avancement < 0)          delta_angle_avancement += 2*Math.PI;
        while (delta_angle_avancement >= 2*Math.PI) delta_angle_avancement -= 2*Math.PI;

        if (delta_angle < Math.PI / 4 && delta_angle_avancement > Math.PI)
        {
            // On sort de l'intersection
            avancementIntersection = -100;
            intersectionArrivee.estLibre = true;
            intersectionArrivee.removeVoiture(this);
            intersectionDepart = intersectionArrivee;

            
            // On passe sur la nouvelle route
            if (routeActuelle.intersectionA == intersectionDepart)
                intersectionArrivee = routeActuelle.intersectionB;
            else
                intersectionArrivee = routeActuelle.intersectionA;
            routeActuelle.addVoiture(this);

            avancementRoute = 0;

            etat = Avancer.Route;
        }
        else
        {
            avancementIntersection -= angle_avancement;
            // Normalement il n'est pas utile de normaliser l'angle
        }  
    }
}
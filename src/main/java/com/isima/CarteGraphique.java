package com.isima;

import java.lang.Math;
import java.io.File;
import java.io.IOException;
import java.awt.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

import javax.vecmath.Vector2d;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;


/** 
 * Classe qui affiche une représentation graphique de la classe Carte grâce à 
 * la bibliothèque standard de dessin en java 
 */
public class CarteGraphique extends JPanel
{   
    double wEcran, hEcran;
    double wVue, hVue;
    double xVueTopLeft, yVueTopLeft;

    // Facteur d'agrandissement appliqué quand un coup de molette est donné
    private double facteurZoom;

    private Carte carte;
    private Color couleurArrierePlan;

    Graphics2D g2;
    BufferedImage img = null;

    private Logger logger = (Logger) LoggerFactory.getLogger("com.isima.logback.CarteGraphique");
    
    CarteGraphique(Carte carte)
    {
        this.carte = carte;

        couleurArrierePlan = new Color(90, 175, 20); // Du vert qui ressemble à de l'herbe

        // Paramètre de zoom
        facteurZoom   = 1.1;

        xVueTopLeft = 50.0;
        yVueTopLeft = 50.0;

        wEcran = 100; 
        hEcran = 100;

        wVue = 0.5*wEcran;
        hVue = 0.5*hEcran;

        if (carte.routesListe.size() > 20) // pour différencier entre les deux cartes et régler la vue de base
        {
            xVueTopLeft = 672; 
            yVueTopLeft = 296; 
            wVue = hVue = 129; 
        }


        try
        {
            img = ImageIO.read(new File("voiture.png"));
        }
        catch (IOException e) 
        {
            logger.error("Erreur du chargement de l'image : " + e.getMessage());
        }
    }

    /**
     * Zoom (ou dézoom) sur la carte en étant centré sur la position de la souris
     * @param sens_zoom : valeur de MouseWheelEvent.getWheelRotation() qui permet 
     * de chosir entre un dézoom (valeur < 0) ou un zoom (valeur > 0)
     * @param x_souris : abscisse de la position de la souris dans la fenêtre
     * @param y_souris : ordonnée de la position de la souris dans la fenêtre
     */
    public void zoomerVersPositionSouris(int sens_zoom, int x_souris, int y_souris)
    {
        Vector2d posSourisEcran = new Vector2d();
        Vector2d posSourisVue   = new Vector2d();

        double echelle2;
        if (sens_zoom < 0) echelle2 = 1 / facteurZoom;
        else               echelle2 = facteurZoom;

        posSourisEcran.x = (double) x_souris;
        posSourisEcran.y = (double) y_souris;
        
        calculerVueCoords(posSourisEcran, posSourisVue);
        
        xVueTopLeft = posSourisVue.x - echelle2 * (posSourisVue.x - xVueTopLeft);
        yVueTopLeft = posSourisVue.y - echelle2 * (posSourisVue.y - yVueTopLeft);

        wVue *= echelle2;
        hVue *= echelle2;

        logger.debug("" + xVueTopLeft +" " + yVueTopLeft + " " + wVue + " " + hVue );
    }   

    /**
     * Déplace la vue selon la différence de pixels entre la nouvelle et l'ancienne 
     * position de la souris
     * @param delta_x : variation des abscisses
     * @param delta_y : variation des ordonnées
     */
    public void translationVue(double delta_x, double delta_y)
    {
        xVueTopLeft -= delta_x * wVue / wEcran;
        yVueTopLeft -= delta_y * hVue / hEcran;
    }

    /**
     * Fonction qui affiche (ou met à jour) le composant à l'écran
     */
    protected void paintComponent(Graphics g)
    {   
        super.paintComponent(g);

        g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        setBackground(couleurArrierePlan);
        
        dessinerRoutes(g);
       
        dessinerIntersections(g);

        dessinerVoitures(g);
    }

    /**
     * Dessine les intersections
     * @param g : objet qui permet de faire du dessin
     */
    private void dessinerIntersections(Graphics g)
    {   
        Vector2d posVue_1   = new Vector2d();
        Vector2d posEcran_1 = new Vector2d();
        double rayon,
              rapport_w = wEcran / wVue,
              rapport_h = hEcran / hVue;

        double largeur_ligne = 0.2;
        Stroke dashed = new BasicStroke((float) (largeur_ligne * rapport_w), BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL,
                                        1, new float[]{6 * (float) rapport_w, 4 * (float) rapport_w}, 0);

        for (Intersection intersection : carte.intersectionsListe)
        {   
            rayon = intersection.getTaille();

            // Dessin du disque principal
            g.setColor(Color.GRAY);
            posVue_1.x = intersection.getX() - rayon;
            posVue_1.y = intersection.getY() - rayon;
            calculerEcranCoords(posVue_1, posEcran_1);
            g.fillOval((int) posEcran_1.x, (int) posEcran_1.y,
                       (int) (2 * rayon * rapport_w), (int) (2 * rayon *  rapport_h));

            // Dessin du disque central
            g.setColor(couleurArrierePlan);
            posVue_1.x = intersection.getX() - rayon/2.5;
            posVue_1.y = intersection.getY() - rayon/2.5;
            calculerEcranCoords(posVue_1, posEcran_1);
            g.fillOval((int) posEcran_1.x, (int) posEcran_1.y,
                       (int) (2.0/2.5 * rayon * rapport_w), (int) (2.0/2.5 * rayon *  rapport_h));

            // Dessin des lignes
            g.setColor(Color.WHITE);
            g2.setStroke(dashed);
            posVue_1.x = intersection.getX() - rayon/1.35;
            posVue_1.y = intersection.getY() - rayon/1.35;
            calculerEcranCoords(posVue_1, posEcran_1);
            g.drawOval((int) posEcran_1.x, (int) posEcran_1.y,
                       (int) (2.0/1.35 * rayon * rapport_w), (int) (2.0/1.35 * rayon *  rapport_h));
        }
    }

    /**
     * Dessine les routes
     * @param g : objet qui permet de faire du dessin
     */
    private void dessinerRoutes(Graphics g)
    {   
        Vector2d posVue_1   = new Vector2d();
        Vector2d posEcran_1 = new Vector2d();
        Vector2d posVue_2   = new Vector2d();
        Vector2d posEcran_2 = new Vector2d();

        double rapport_w = wEcran / wVue;

        //g2.setStroke();
        double largeur_route = carte.routesListe.get(0).getLargeur();
        double largeur_ligne = 0.2;
        Stroke routeStroke = new BasicStroke((float) (largeur_route * rapport_w)); // Normalement c'est plus compliqué que ça mais bon ...
                                                                             // i.e il faudait calculer la dilatation sur les composantes
                                                                             // d'un vecteur orthoganal à la droite puis ... enfin bref
        Stroke dashed = new BasicStroke((float) (largeur_ligne * rapport_w), BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL,
                                        1, new float[]{6 * (float)rapport_w, 4 * (float) rapport_w}, 0);

        for (Route route : carte.routesListe)
        {   
            posVue_1.x = route.intersectionA.getX();
            posVue_1.y = route.intersectionA.getY();
            posVue_2.x = route.intersectionB.getX();
            posVue_2.y = route.intersectionB.getY();
            
            calculerEcranCoords(posVue_1, posEcran_1);
            calculerEcranCoords(posVue_2, posEcran_2);
            
            // Dessin de la chaussée
            g.setColor(Color.GRAY);
            g2.setStroke(routeStroke);
            g2.drawLine((int) posEcran_1.x, (int) posEcran_1.y, 
                        (int) posEcran_2.x, (int) posEcran_2.y);
            
            // Dessin de la ligne centrale
            g.setColor(Color.WHITE);
            g2.setStroke(dashed);
            g2.drawLine((int) posEcran_1.x, (int) posEcran_1.y, 
                        (int) posEcran_2.x, (int) posEcran_2.y);

        }
    }

    /** 
     * Fait avancer les voitures sur la carte
     */
    public void faireAvancerVoitures()
    {   
        for (Voiture voiture : carte.voituresListe)
        {
            voiture.avancer();
        }
    }

    /**
     * Dessine les voitures
     * @param g : objet qui permet de faire du dessin
     */
    private void dessinerVoitures(Graphics g)
    {   
        Vector2d vect_direction  = new Vector2d();
        Vector2d vect_ortho      = new Vector2d();
        Vector2d pos_objet       = new Vector2d();
        Vector2d pos_objet_ecran = new Vector2d();

        double 
            rapport_w = wEcran / wVue,
            rapport_h = hEcran / hVue;

        // Dessin des voitures sur la route
        for (Route route : carte.routesListe)
        {
            g.setColor(Color.RED);
            for (Voiture voiture : route.getVoituresListe())
            {   
                // Calcul du vecteur de déplacement
                Intersection inter_depart = voiture.getIntersectionDepart();
                Intersection inter_arrivee = voiture.getIntersectionArrivee();

                vect_direction.setX(inter_arrivee.getX() - inter_depart.getX());
                vect_direction.setY(inter_arrivee.getY() - inter_depart.getY());

                vect_direction.normalize();
                vect_ortho.set(-vect_direction.y, vect_direction.x);

                pos_objet.x = inter_depart.x  + (inter_depart.getTaille() + voiture.getAvancementRoute())*vect_direction.x
                    + vect_ortho.x*(route.getLargeur() / 4 + 2.5);

                pos_objet.y = inter_depart.y  + (inter_depart.getTaille() + voiture.getAvancementRoute())*vect_direction.y
                   + vect_ortho.y*(route.getLargeur() / 4 + 2.5);

                // Calcul de la position de la voiture à l'écran
                calculerEcranCoords(pos_objet, pos_objet_ecran);
                
                // On est obligé de faire une copie de g pour éviter les conflits
                Graphics2D g2d = (Graphics2D) g.create();
                g2d.rotate(Math.atan2(-vect_direction.y, -vect_direction.x), pos_objet_ecran.x, pos_objet_ecran.y);
                   
                /*g2d.fillRect(
                    (int) (pos_objet_ecran.x),
                    (int) (pos_objet_ecran.y),
                    (int) (voiture.getTaille() * rapport_w), (int) (5 * rapport_h));*/

                g2d.drawImage(img, 
                    (int) (pos_objet_ecran.x),
                    (int) (pos_objet_ecran.y),
                    (int) (pos_objet_ecran.x + voiture.getTaille() * rapport_w),
                    (int) (pos_objet_ecran.y + 5 * rapport_h), 
                    0,0, 516, 255,null);

                g2d.dispose();
            }
        }

        // Dessins des voitures sur les intersections
        for (Intersection intersection : carte.intersectionsListe)
        {
            for (Voiture voiture : intersection.getVoituresListe())
            {
                pos_objet.x = (intersection.getX() + Math.cos(voiture.getAvancementIntersection())*intersection.getTaille()*0.7);
                pos_objet.y = (intersection.getY() + Math.sin(voiture.getAvancementIntersection())*intersection.getTaille()*0.7);
                calculerEcranCoords(pos_objet, pos_objet_ecran);
                
                // On est obligé de faire une copie de g pour éviter les conflits
                Graphics2D g2d = (Graphics2D) g.create();
                g2d.rotate(voiture.getAvancementIntersection() - Math.PI/2, pos_objet_ecran.x, pos_objet_ecran.y);
                /*g2d.fillRect(
                    (int) (pos_objet_ecran.x - voiture.getTaille() / 2),
                    (int) (pos_objet_ecran.y - voiture.getTaille() / 2),
                    (int) (voiture.getTaille() * rapport_w), (int) (5 * rapport_h));*/

                g2d.drawImage(img, 
                    (int) (pos_objet_ecran.x - voiture.getTaille() / 2),
                    (int) (pos_objet_ecran.y - voiture.getTaille() / 2),
                    (int) (pos_objet_ecran.x - voiture.getTaille() / 2 + voiture.getTaille() * rapport_w),
                    (int) (pos_objet_ecran.y - voiture.getTaille() / 2 + 5 * rapport_h), 
                    0,0, 516, 255,null);

                g2d.dispose();
            }
        }
    }

    /**
     * Calcule les coordonnés d'un point dans le repère écran vers le repère de
     * base
     * @param[in] posEcran : point dans le repère écran
     * @param[out] posVue : point dans le repère de base
     */
    private void calculerVueCoords(Vector2d posEcran, Vector2d posVue)
    {
        posVue.x = posEcran.x * wVue / wEcran + xVueTopLeft;
        posVue.y = posEcran.y * hVue / hEcran + yVueTopLeft;
    }

    /**
     * Calcule les coordonnés d'un point dans le repère de base vers le repère
     * écran
     * @param[in] posVue : point dans le repère de base
     * @param[out] posEcran : point dans le repère écran
     */
    private void calculerEcranCoords(Vector2d posVue, Vector2d posEcran)
    {
        posEcran.x = (posVue.x - xVueTopLeft) * wEcran / wVue;
        posEcran.y = (posVue.y - yVueTopLeft) * hEcran / hVue;
    }
}
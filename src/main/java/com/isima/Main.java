package com.isima;


import javax.swing.SwingUtilities;
import javax.swing.JFrame;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

/**
 * Point d'entrée du programme
 */
public class Main
{  
    static Logger parentLogger;

    public static void main(String[] args)
    {  
        parentLogger = (Logger) LoggerFactory.getLogger("com.isima.logback");
        parentLogger.setLevel(Level.INFO);

        SwingUtilities.invokeLater(
            new Runnable() 
            {
                public void run(){lancerApplication();}
            }
        );
    }

    private static void lancerApplication()
    {
        JFrame fenetre = new JFrame();
        fenetre.setTitle("Simulation de traffic routier");
        fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        fenetre.setSize(700, 700);
        
        Carte carte = new Carte();
        CarteGraphique carteGraphique = new CarteGraphique(carte);
        GestionnaireEvenements gestionnaireEvenements = new GestionnaireEvenements(carteGraphique);

        fenetre.addMouseWheelListener(gestionnaireEvenements);
        fenetre.addMouseMotionListener(gestionnaireEvenements); 
        fenetre.add(carteGraphique);
        fenetre.setVisible(true);


        parentLogger.info("Working Directory : " + System.getProperty("user.dir"));
        parentLogger.info("Created GUI on EDT ? " + SwingUtilities.isEventDispatchThread());
        parentLogger.info("Double buffer ? " + fenetre.isDoubleBuffered());
    }
}
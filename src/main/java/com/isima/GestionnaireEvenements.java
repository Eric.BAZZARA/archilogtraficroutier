package com.isima;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;
import javax.vecmath.Vector2d;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

/**
 * Classe qui sert de contrôleur dans le modèle MVC, elle collecte tous
 * les évènements de l'utilisateur à travers l'interface graphique
 */
public class GestionnaireEvenements implements MouseWheelListener, MouseMotionListener
{   
    private Logger logger;

    private CarteGraphique carteGraphique;
    private Vector2d oldPosSouris, newPosSouris;

    GestionnaireEvenements(CarteGraphique carteGraphique)
    {   
        logger = (Logger) LoggerFactory.getLogger("com.isima.logback.GestionnaireEvenements");

        this.carteGraphique = carteGraphique;

        oldPosSouris = new Vector2d(-1.0, -1.0);
        newPosSouris = new Vector2d(0.0, 0.0);

        ActionListener taskPerformer = new ActionListener() 
        {   
            int i;

            public void actionPerformed(ActionEvent evt)
            {
              logger.debug("Timer is running " + i);
              i++;
              carteGraphique.faireAvancerVoitures();
              carteGraphique.repaint();
            }
        };

        int delais_images = 50; // Temps entre deux rafraîchissement d'images

        Voiture.setRapportDeVitesse(1000.0 / (double) delais_images);

        Timer timer = new Timer(delais_images, taskPerformer);
        timer.setInitialDelay(100);
        timer.setRepeats(true);
        timer.start();

    }

    /* Implémentation de l'interface MouseWheelListener */
    public void mouseWheelMoved(MouseWheelEvent e) 
    {
        carteGraphique.zoomerVersPositionSouris(e.getWheelRotation(), e.getX(), e.getY());
        carteGraphique.repaint();
    }

    /* Implémentation de l'interface MouseMotionListener */
    public void mouseDragged(MouseEvent e)
    {   
        
        // Si cette condition est vérifiée alors cela signifie que l'on commence
        // une translation de la vue et alors on initialise l'ancienne position
        // de la souris avec celle de l'actuelle
        if (oldPosSouris.x == -1.0)
        {   
            oldPosSouris.x = (double) e.getX();
            oldPosSouris.y = (double) e.getY();
        }

        newPosSouris.x = (double) e.getX();
        newPosSouris.y = (double) e.getY();

        carteGraphique.translationVue(newPosSouris.x - oldPosSouris.x,
                                      newPosSouris.y - oldPosSouris.y);
        oldPosSouris.x = newPosSouris.x;
        oldPosSouris.y = newPosSouris.y;

        carteGraphique.repaint();
    }

    /* Implémentation de l'interface MouseMotionListener */
    public void mouseMoved(MouseEvent e)
    {
        oldPosSouris.x = -1.0; // On réinitialise la position de la souris enregistrée
    }
}

package com.isima;

/**
 * Classe qui modélise une carte et qui stocke tous les éléments qui la 
 * constitue (routes, intersections, voitures)
 */
public class Carte
{   
    java.util.ArrayList<Route> routesListe;
    java.util.ArrayList<Intersection> intersectionsListe;
    java.util.ArrayList<Voiture> voituresListe;


    Carte()
    {   
        routesListe        = new java.util.ArrayList<Route>();
        intersectionsListe = new java.util.ArrayList<Intersection>();
        voituresListe      = new java.util.ArrayList<Voiture>();

        creerCarte_2();
    }

    /**
     * Méthode commode qui crée une intersection et l'ajoute à la liste des intersections
     * @param x : abscisse du centre de l'intersection
     * @param y : ordonnée du centre de l'intersection
     * @return l'intersection créée
     */
    private Intersection creerIntersection(double x, double y)
    {
        Intersection intersection = new Intersection(x, y);
        intersectionsListe.add(intersection);

        return intersection;
    }

    /**
     * Méthode commode qui crée une route et l'ajoute à la liste des routes
     * @param inter_1 : première intersection
     * @param inter_2 : deuxième intersection
     * @return la route créée
     */
    private Route creerRoute(Intersection inter_1, Intersection inter_2)
    {
        Route route = new Route(inter_1, inter_2);
        routesListe.add(route);

        return route;
    }

    /**
     * Méthode qui crée une voiture et l'ajoute sur la carte
     * @param route : route sur laquelle placer la voiture
     * @param sens : définit l'intersection d'arrivée de la voiture (et donc
     * son sens). Si sens = 0, l'arrivée est route.intersectionA sinon c'est
     * route.intersectionB.
     * @param avancement : nombre entre 0 et 1 qui place la voiture précisement
     * sur la route. Si avancement = 0.0, la voiture est au tout début de la 
     * route, et si avancement = 1.0 la voiture est tout à la fin.
     * @return la voiture créée
     */
    private Voiture creeVoiture(Route route, int sens, double avancement)
    {
        double vitesse_voitures = 40;
        Intersection arrivee, depart;

        if (sens == 0){
            arrivee = route.intersectionA;
            depart  = route.intersectionB;
        }
        else{
            arrivee = route.intersectionB;
            depart  = route.intersectionA;
        }

        Voiture voiture = new Voiture(vitesse_voitures, route, depart, arrivee);
        voiture.setAvancementRoute(route.getDistance() * avancement);
        voituresListe.add(voiture);

        return voiture;
    }

    /**
     *  Créer une carte simple avec 5 intersections et 6 routes
     */
    private void creerCarte_1()
    {
        Intersection inter_1 = creerIntersection(100, 100);
        Intersection inter_2 = creerIntersection(200, 150);
        Intersection inter_3 = creerIntersection(150, 300);
        Intersection inter_4 = creerIntersection(300, 150);
        Intersection inter_5 = creerIntersection(300, 300);

        Route route_1 = creerRoute(inter_1, inter_2);
        Route route_2 = creerRoute(inter_1, inter_3);
        Route route_3 = creerRoute(inter_3, inter_2);
        creerRoute(inter_4, inter_2);
        creerRoute(inter_4, inter_5);
        creerRoute(inter_3, inter_5);

        // Création de voitures
        for (int k = 0; k < 4; k++){
            creeVoiture(route_1, 0, k / 4.0);
        }

        for (int k = 0; k < 5; k++){
            creeVoiture(route_2, 1, k / 5.0);
        }

        for (int k = 0; k < 10; k++){   
            creeVoiture(route_3, 1, k / 10.0);
        }
    }

    private void creerCarte_2()
    {
        Intersection i1 = creerIntersection(819, 110);
        Intersection i2 = creerIntersection(874, 128);
        Intersection i3 = creerIntersection(1094, 257);
        Intersection i4 = creerIntersection(1143, 294);
        Intersection i5 = creerIntersection(826, 583);
        Intersection i6 = creerIntersection(757, 640);
        Intersection i7 = creerIntersection(694, 686);
        Intersection i8 = creerIntersection(340, 800);
        Intersection i9 = creerIntersection(202, 581);
        Intersection i10 = creerIntersection(83, 543);
        Intersection i11 = creerIntersection(65, 476);
        Intersection i12 = creerIntersection(178, 269);
        Intersection i13 = creerIntersection(326, 251);
        Intersection i14 = creerIntersection(405, 208);
        Intersection i15 = creerIntersection(439, 188);
        Intersection i16 = creerIntersection(486, 163);
        Intersection i17 = creerIntersection(635, 64);
        Intersection i56 = creerIntersection(700, 10);

        Intersection i18 = creerIntersection(420, 493);
        Intersection i19 = creerIntersection(417, 604);
        Intersection i20 = creerIntersection(402, 694);
        Intersection i21 = creerIntersection(425, 722);
        Intersection i22 = creerIntersection(487, 734);
        Intersection i23 = creerIntersection(529, 712);
        Intersection i24 = creerIntersection(567, 605);
        Intersection i25 = creerIntersection(537, 500);
        Intersection i26 = creerIntersection(617, 460);
        Intersection i27 = creerIntersection(645, 547);
        Intersection i28 = creerIntersection(784, 509);
        Intersection i29 = creerIntersection(725, 472);
        Intersection i30 = creerIntersection(706, 424);
        Intersection i31 = creerIntersection(850, 367);

        Intersection i32 = creerIntersection(885, 185);
        Intersection i33 = creerIntersection(722, 258);
        Intersection i34 = creerIntersection(658, 277);
        Intersection i36 = creerIntersection(648, 233);
        Intersection i37 = creerIntersection(596, 225);
        Intersection i38 = creerIntersection(654, 145);
        Intersection i54 = creerIntersection(613, 257);
        Intersection i55 = creerIntersection(650, 210);
        Intersection i39 = creerIntersection(515, 234);
        Intersection i40 = creerIntersection(551, 339);
        Intersection i41 = creerIntersection(512, 355);
        Intersection i42 = creerIntersection(471, 352);
        Intersection i43 = creerIntersection(423, 403);
        Intersection i44 = creerIntersection(364, 432);
        Intersection i45 = creerIntersection(350, 384);
        Intersection i46 = creerIntersection(375, 371);
        Intersection i47 = creerIntersection(404, 351);
        Intersection i48 = creerIntersection(429, 343);
        Intersection i49 = creerIntersection(451, 327);
        Intersection i50 = creerIntersection(431, 280);
        Intersection i51 = creerIntersection(419, 246);
        Intersection i52 = creerIntersection(455, 227);
        Intersection i53 = creerIntersection(467, 261);
        Intersection i57 = creerIntersection(657, 350);

        for (Intersection intersection : intersectionsListe)
        {
            intersection.x *= 2;
            intersection.y *= 2;
        }




        creerRoute(i1, i2);
        creerRoute(i2, i3);
        creerRoute(i3, i4);
        creerRoute(i4, i5);
        creerRoute(i5, i6);
        creerRoute(i6, i7);
        creerRoute(i7, i8);
        creerRoute(i8, i9);
        creerRoute(i9, i10);
        creerRoute(i10, i11);
        creerRoute(i11, i12);
        creerRoute(i12, i13);
        creerRoute(i13, i14);
        creerRoute(i14, i15);
        creerRoute(i15, i16);
        creerRoute(i16, i17);
        creerRoute(i17, i56);
        creerRoute(i56, i1);

        creerRoute(i9, i18);
        creerRoute(i18, i19);
        creerRoute(i19, i20);
        creerRoute(i20, i21);
        creerRoute(i21, i22);
        creerRoute(i22, i23);
        creerRoute(i23, i24);
        creerRoute(i24, i25);
        creerRoute(i19, i25);
        creerRoute(i25, i26);
        creerRoute(i26, i27);
        creerRoute(i27, i7);
        creerRoute(i27, i6);
        creerRoute(i27, i29);
        creerRoute(i28, i29);
        creerRoute(i28, i5);
        creerRoute(i29, i30);
        creerRoute(i30, i26);
        creerRoute(i29, i31);
        creerRoute(i31, i3);
        creerRoute(i28, i3);

        creerRoute(i18, i44);
        creerRoute(i18, i41);
        creerRoute(i40, i41);
        creerRoute(i26, i40);
        creerRoute(i30, i57);
        creerRoute(i2, i32);
        creerRoute(i32, i33);
        creerRoute(i33, i34);
        creerRoute(i34, i36);
        creerRoute(i36, i55);
        creerRoute(i55, i37);
        creerRoute(i37, i54);
        creerRoute(i54, i36);
        creerRoute(i55, i38);
        creerRoute(i38, i17);
        creerRoute(i38, i1);
        creerRoute(i57, i34);
        creerRoute(i57, i40);
        creerRoute(i41, i42);
        creerRoute(i42, i43);
        creerRoute(i42, i49);
        creerRoute(i43, i44);
        creerRoute(i44, i45);
        creerRoute(i45, i46);
        creerRoute(i46, i47);
        creerRoute(i47, i48);
        creerRoute(i48, i49);
        creerRoute(i49, i50);
        creerRoute(i50, i51);
        creerRoute(i51, i14);
        creerRoute(i51, i52);
        creerRoute(i52, i15);
        creerRoute(i52, i53);
        creerRoute(i53, i39);
        creerRoute(i39, i40);
        creerRoute(i45, i13);
        creerRoute(i43, i47);
        creerRoute(i50, i53);
        creerRoute(i16, i39);
        creerRoute(i31, i33);


        for (Route route : routesListe)
        {
            for (int k = 0; k < 2; k++){
                creeVoiture(route, 0, (k + 1) / 4);
            }
        }

    }
}

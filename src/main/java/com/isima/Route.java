package com.isima;

import java.util.ArrayList;

import javax.vecmath.Vector2d;

/**
 * Classe qui modélise une route (sous forme de deux intersections) 
 */
public class Route
{
    Intersection intersectionA;
    Intersection intersectionB;
    private double distance; // Entre le début de chaque intersection, pas entre les centres d'intersection
    private double largeur; // Largueur de la route
    private double vitesseMaxAutorisee; // Limite de vitesse sur la route

    private int id;

    private ArrayList<Voiture> voituresListe;

    Route(Intersection intersectionA, Intersection intersectionB)
    {   
        
        this.intersectionA = intersectionA;
        this.intersectionB = intersectionB;
        this.intersectionA.addRouteListe(this);
        this.intersectionB.addRouteListe(this);
        
        vitesseMaxAutorisee = 50;
        this.largeur = 10;
        this.id = DistributeurId.getNewId();

        this.voituresListe = new java.util.ArrayList<Voiture>();

        // Calcul de la distance de la route
        Vector2d direction_route = new Vector2d(
            intersectionB.getX() - intersectionA.getX(), 
            intersectionB.getY() - intersectionA.getY());

        double norme = direction_route.length();
        
        this.distance = norme - intersectionA.getTaille() - intersectionB.getTaille();
    }

    /**
     * @return l'identifiant de la route
     */
    public int getId(){
        return id;
    }

    /**
     * @return la distance de la route
     */
    public double getDistance(){
        return distance;
    }

    /**
     *  @return la largeur de la route
     */
    public double getLargeur(){
        return largeur;
    }

    /**
     * @return la vitesse maximale autorisée sur la route
     */
    public double getVitesseMaxAutorisee(){
        return vitesseMaxAutorisee;
    }

    /**
     * @return la liste des voitures présentes sur la route
     */
    public ArrayList<Voiture> getVoituresListe()
    {
        return voituresListe;
    }

    /**
     * Ajoute une voiture sur la route
     * @param voiture : la voiture à ajouter
     */
    public void addVoiture(Voiture voiture)
    {
        voituresListe.add(voiture);
    }

    /**
     * Enlève une voiture sur la route
     * @param voiture : la voiture à enlever
     */
    public void removeVoiture(Voiture voiture)
    {
        voituresListe.remove(voiture);
    }

    @Override
    public String toString()
    {
        return "Route " + this.id + " (" 
            + this.intersectionA.toString() + ", " + this.intersectionB.toString() + ")";
    }
}
package com.isima;

/**
 * Mini-classe qui donne un identifiant unique à tout objet en demandant un.
 * Cela permet de comparer des objets plus proprement en faisant un test de 
 * type : obj1.getId() == obj2.getId() plutôt que de directement faire : obj1
 *  == obj2. Cela permet aussi d'utiliser le patron singleton.
 */
public class DistributeurId 
{
    private static int id = 0;

    public static int getNewId()
    {
        id++;

        return id;
    }
}
